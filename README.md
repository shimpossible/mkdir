# mkdir 1.0

 What is this?

A working version of mkdir that supports the "-p" command on windows.

I've tried the one at http://gnuwin32.sourceforge.net/packages/coreutils.htm but there is a bug in it.
mkdir is simple enough to just write it from scratch