#include <stdio.h>
#include <Windows.h>
#include <tchar.h>

#define PROGRAM_NAME _T("mkdir")

bool parent = false;
bool verbose = false;
int create_dir(_TCHAR* dir)
{
	DWORD status = CreateDirectory(dir, 0);
	if (status == 0)
	{
		DWORD e = GetLastError();

		// recurisve shouldn't care
		if (parent && e == ERROR_ALREADY_EXISTS) 
			return 0;

		_tprintf(_T("%s: cannot create directory '%s'"), PROGRAM_NAME, dir);
		switch (e)
		{
		case ERROR_ALREADY_EXISTS: 			
			_tprintf(_T("File exists\r\n")); break;
		case ERROR_PATH_NOT_FOUND: _tprintf(_T("No such file or directory\r\n")); break;
		}
		return -1;
	}
	else
	{
		if(verbose) _tprintf(_T("%s: created directory '%s'\r\n"), PROGRAM_NAME, dir);
	}
	return 0;
}

void display_help()
{
	_tprintf(_T("Usage %s [OPTION] DIRECTORY\r\n"), PROGRAM_NAME);
	_tprintf(_T("\
  -m, --mode=MODE   set file mode (as in chmod), not a=rwx - umask\r\n\
  -p, --parents     no error if existing, make parent directories as needed\r\n\
  -v, --verbose     print a message for each created directory\r\n\
      --version     display version\r\n\
      --help        this screen\r\n\
"));
}

void display_version()
{
	_tprintf(_T("%s version 1.0\r\n"), PROGRAM_NAME);
}

int _tmain(int argc, _TCHAR* argv[])
{
	_TCHAR* dest = 0;

	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == '-')
		{
			if (_tcscmp(_T("-p"), argv[i])  == 0 ||
				_tcscmp(_T("--parent"), argv[i]) == 0
				)
			{
				parent = true;
			}
			if (_tcscmp(_T("-v"), argv[i]) == 0 ||
				_tcscmp(_T("--verbose"), argv[i]) == 0
				)
			{
				verbose = true;
			}
			if (_tcscmp(_T("--version"), argv[i]) == 0)
			{
				display_version();
				return 0;
			}
			if (_tcscmp(_T("--help"), argv[i]) == 0)
			{
				display_help();
				return 0;
			}
		}
		else
		{
			dest = argv[i];
			break;
		}
	}

	if (dest == 0)
	{
		_tprintf(_T("%s: missing operand\r\n"), PROGRAM_NAME);
		_tprintf(_T("Try '%s --help' for more information.\r\n"), PROGRAM_NAME);
		return -1;
	}

	if (parent)
	{
		for (size_t i = 1; i < _tcslen(dest); i++)
		{
			if (dest[i] == '/' || dest[i] == '\\')
			{
				dest[i] = 0;
				if (create_dir(dest))
				{
					// some type of error
					return -1;
				}
				dest[i] = '/';
			}
		}
	}

	return create_dir(dest);
}